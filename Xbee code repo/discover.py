from digi.xbee.models.options import DiscoveryOptions
from digi.xbee.models.status import NetworkDiscoveryStatus
import time


class Discovery:

    def __init__(self, device):
        self.local_uart = device
        self.xnet = self.local_uart.get_network()
        self.node_list = []
        self.discover_mode()

    def getter(self):
        return self.node_list

    def discover_mode(self):
        try:
            self.xnet.set_discovery_options({DiscoveryOptions.DISCOVER_MYSELF})
            self.xnet.set_discovery_timeout(5)
            self.xnet.clear()
            self.xnet.add_device_discovered_callback(self.callback_device_discovered)
            self.xnet.add_discovery_process_finished_callback(self.callback_discovery_finished)
            self.xnet.start_discovery_process()
            print("searching for xbee devices")
            while self.xnet.is_discovery_running():
                time.sleep(0.5)

        except AttributeError:
            print("Check if xbee is connected!")

    def callback_device_discovered(self, remote):
        print("This device was found:", remote)
        remote_str = str(remote)
        self.node_list.append("MAC " + remote_str.split(" - ")[0])
        self.node_list.append("ID " + remote_str.split(" - ")[1])

    def callback_discovery_finished(self, status):
        if status == NetworkDiscoveryStatus.SUCCESS:
            callback_finish = "There was no problems detected during discovery"
            print(callback_finish)
            self.xnet.clear()

        else:
            print("Something went wrong with discovery: %s %status.description")
