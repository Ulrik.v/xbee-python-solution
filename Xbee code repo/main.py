from digi.xbee.devices import XBeeDevice
from findserial import SerialFinder
from discover import Discovery
from gateway_printer import WriteNodes
from userinput import UserDecision
from receive import Receiver
from send import Sender


class Controller:

    def __init__(self):

        self.my_remote_list = []
        self.port_method()
        self.uart_device = XBeeDevice(self.my_remote_list[1], self.my_remote_list[0])
        self.serial_open_method()
        self.discover_method()
        self.discovered_nodes_method()
        self.user_choice_method()
        self.send_and_receive_method()
        self.serial_close_method()

    def port_method(self):
        port_module = SerialFinder()
        self.my_remote_list = port_module.serial_available()
        print("Baud rate chosen %s and serial port chosen %s" % (self.my_remote_list[0], self.my_remote_list[1]))
        del port_module

    def serial_open_method(self):
        try:
            self.uart_device.open()
            print("Serial has opened")
        except AttributeError:
            print("Make sure Zigbee is connected to a serial port")

    def discover_method(self):
        discovery_module = Discovery(self.uart_device)
        self.discovered_nodes = discovery_module.getter()
        del discovery_module

    def discovered_nodes_method(self):
        WriteNodes(self.discovered_nodes)

    def user_choice_method(self):
        while True:
            try:
                user_choice = int(input("Enter desired destination node, by using indexing starting from 0: "))
            except ValueError:
                print("Please chose with indexing from 0 and up")
            else:
                user_input = UserDecision()
                self.xbee_chosen = user_input.xbee_to_pick(user_choice)
                del user_input
                break

    def send_and_receive_method(self):
        self.receive_module = Receiver(self.uart_device)
        while True:
            self.message = input("Enter the message to send: ")
            if self.message is not None and self.message != "Select()" and self.message != "Quit()":
                self.send_module = Sender(self.uart_device, self.xbee_chosen)
                self.send_module.sender(self.message)
                self.message = None
            elif self.message == "Select()":
                self.user_choice_method()
            elif self.message == "Quit()":
                print("User have requested to stop program")
                break

    def serial_close_method(self):
        self.uart_device.close()
        print("Local device is now closed. Closing program...")


if __name__ == "__main__":
    Co = Controller()
