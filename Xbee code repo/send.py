from digi.xbee.devices import RemoteXBeeDevice
from digi.xbee.devices import XBee64BitAddress
from time import sleep


class Sender:

    def __init__(self, device, xbee_chosen):
        self.local_uart = device
        self.remote_device = RemoteXBeeDevice(self.local_uart, XBee64BitAddress.from_hex_string(xbee_chosen))

    def sender(self, message):
        if message == "":
            print("Cannot send empty message!")
            return
        else:
            print("Sending data to %s >> %s..." % (self.remote_device.get_64bit_addr(), message))
            self.local_uart.send_data(self.remote_device, message)
            print("Success! use Select() to chose another destination device and Quit() to close program at next "
                  "enter the message prompt")
            print("Timeout for 15 seconds begins now, waiting for a reply...")
            sleep(15)  # Gives destination devices 15 minutes to reply then goes back to send loop
