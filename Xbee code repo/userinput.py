class UserDecision:

    def __init__(self):
        self.MAC_list = []

    def xbee_to_pick(self, user_choice):
        user_choice = user_choice
        try:
            f = open("Xbee_network_list.txt", "r")
            # reading each line
            for line in f:
                # reading each word
                for word in line.split():
                    # putting each MAC address in a list
                    self.MAC_list.append(word)

            device_to_send_data = self.MAC_list[user_choice]
            return device_to_send_data
            # Returns the chosen MAC from the created list
        except IndexError:
            print("Xbee chosen was out of the saved list, check the list again. Rerun the program and try again")
            exit()
