import datetime


class Receiver:

    def __init__(self, device):
        self.local_uart = device
        self.local_uart.add_data_received_callback(self.data_receive_callback)

    def data_receive_callback(self, xbee_message):
        get_realtime = datetime.datetime.now()
        print("From: %s >> %s" % (xbee_message.remote_device.get_64bit_addr(),
                                  xbee_message.data.decode("ascii")), ">> Received:", get_realtime)
